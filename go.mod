module notabug.org/apiote/bimba_cli

go 1.16

require (
	apiote.xyz/p/szczanieckiej v0.0.0-20230519082024-660bdb7c571c
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/ulikunitz/xz v0.5.11 // indirect
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)

replace apiote.xyz/p/szczanieckiej => /home/adam/Code/current/szczanieckiej/
