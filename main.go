package main

import (
	"fmt"
	"net/http"
	urllib "net/url"
	"os"
	"time"

	"apiote.xyz/p/szczanieckiej/api"
	"git.sr.ht/~sircmpwn/go-bare"
)

var host = "http://127.0.0.1:51354"
var url = host + "/poznan_ztm/"

func getFeeds() {
	client := &http.Client{}
	req, err := http.NewRequest("GET", host, nil)
	req.Header.Add("Accept", "application/1+bare")
	response, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Getting")
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer response.Body.Close()
	b := bare.NewReader(response.Body)
	tag, _ := b.ReadUint()
	switch tag {
	case 0:
		l, _ := b.ReadUint()
		feeds := make([]api.FeedInfoV2, l)
		for i := 0; i < int(l); i++ {
			n, _ := b.ReadString()
			id, _ := b.ReadString()
			a, _ := b.ReadString()
			d, _ := b.ReadString()
			l, _ := b.ReadString()
			qh, _ := b.ReadString()
			qi, _ := b.ReadUint()
			qs, _ := b.ReadString()
			vs, _ := b.ReadString()
			leng, _ := b.ReadUint()
			vt := make([]byte, leng)
			b.ReadDataFixed(vt)
			feedInfo := api.FeedInfoV2{
				n, id, a, d, l, qh, api.QRLocationV1(qi), qs, vs, string(vt),
			}
			feeds[i] = feedInfo
		}
		fmt.Printf("%+v\n", feeds)
	case 1:
		l, _ := b.ReadUint()
		feeds := make([]api.FeedInfoV1, l)
		for i := 0; i < int(l); i++ {
			n, _ := b.ReadString()
			id, _ := b.ReadString()
			a, _ := b.ReadString()
			d, _ := b.ReadString()
			leng, _ := b.ReadUint()
			lu := make([]byte, leng)
			b.ReadDataFixed(lu)
			feedInfo := api.FeedInfoV1{
				n, id, a, d, string(lu),
			}
			feeds[i] = feedInfo
		}
		fmt.Printf("%+v\n", feeds)
	default:
		fmt.Println("unknown tag", tag)
	}
}

func getDepartures(code string) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url+"departures?code="+code, nil)
	req.Header.Add("Accept", "application/0+bare")
	response, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Getting")
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer response.Body.Close()
	b := bare.NewReader(response.Body)
	tag, _ := b.ReadUint()
	switch tag {
	case 0:
		now := time.Now()
		l, _ := b.ReadUint()
		alerts := make([]api.AlertV1, l)
		for i := 0; i < int(l); i++ {
			h, _ := b.ReadString()
			d, _ := b.ReadString()
			u, _ := b.ReadString()
			c, _ := b.ReadUint()
			e, _ := b.ReadUint()
			alerts[i] = api.AlertV1{
				h, d, u, api.AlertCauseV1(c), api.AlertEffectV1(e),
			}
		}

		l, _ = b.ReadUint()
		departures := make([]api.DepartureV3, l)
		for i := 0; i < int(l); i++ {
			id, _ := b.ReadString()
			h, _ := b.ReadU8()
			m, _ := b.ReadU8()
			s, _ := b.ReadU8()
			dO, _ := b.ReadI8()
			z, _ := b.ReadString()
			vS, _ := b.ReadUint()
			r, _ := b.ReadBool()
			v_id, _ := b.ReadString()
			v_lat, _ := b.ReadF64()
			v_lon, _ := b.ReadF64()
			v_c, _ := b.ReadU16()
			v_s, _ := b.ReadF32()
			v_l_n, _ := b.ReadString()
			v_l_k, _ := b.ReadUint()
			v_l_c_r, _ := b.ReadU8()
			v_l_c_g, _ := b.ReadU8()
			v_l_c_b, _ := b.ReadU8()
			v_h, _ := b.ReadString()
			v_cL, _ := b.ReadUint()
			v_cS, _ := b.ReadUint()
			board, _ := b.ReadU8()
			departures[i] = api.DepartureV3{
				id, api.TimeV1{h, m, s, dO, z}, api.VehicleStatusV1(vS), r, api.VehicleV3{v_id, api.PositionV1{v_lat, v_lon}, v_c, v_s, api.LineStubV3{v_l_n, api.LineTypeV3(v_l_k), api.ColourV1{v_l_c_r, v_l_c_g, v_l_c_b}}, v_h, api.CongestionLevelV1(v_cL), api.OccupancyStatusV1(v_cS)}, board,
			}
		}
		stop := readStop(b)

		fmt.Printf("%+v\n", stop)
		fmt.Println(alerts)
		for _, departure := range departures {
			departureTime := time.Date(now.Year(), now.Month(), now.Day(), int(departure.Time.Hour), int(departure.Time.Minute), int(departure.Time.Second), 0, now.Location())
			dayOffset, _ := time.ParseDuration(fmt.Sprintf("%dh", departure.Time.DayOffset*24))
			departureTime = departureTime.Add(dayOffset)
			timeTo := int(departureTime.Sub(now).Minutes())
			fmt.Printf("in %d minutes (%02d:%02d:%02d) RT:%v Status:%v Board %v\n", timeTo, departure.Time.Hour, departure.Time.Minute, departure.Time.Second, departure.IsRealtime, departure.Status, departure.Boarding)
			fmt.Printf("\t%+v\n", departure.Vehicle)
		}
	default:
		fmt.Printf("unknown type\n")
	}
}

func query(q string, param string) {
	client := &http.Client{}
	q = urllib.QueryEscape(q)
	req, err := http.NewRequest("GET", url+"queryables?"+param+"="+q, nil)
	req.Header.Add("Accept", "application/0+bare")
	response, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Getting")
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer response.Body.Close()
	b := bare.NewReader(response.Body)
	tag, _ := b.ReadUint()
	switch tag {
	case 0:
		length, _ := b.ReadUint()
		if length == 1 {
			getDepartures(q)
		} else {
			for i := 0; i < int(length); i++ {
				b.ReadUint()
				if tag == 0 {
					stop := readStop(b)
					fmt.Printf("%s (%s) [%s]\n", stop.Name, stop.Code, stop.NodeName)
					for _, option := range stop.ChangeOptions {
						fmt.Printf("\t%s -> %s\n", option.LineName, option.Headsign)
					}
				} else {
					line := readLine(b)
					fmt.Printf("%s [%v], %s, %v\n", line.Name, line.Colour, line.Kind, line.Headsigns)
					fmt.Printf("%+v\n", line)
				}
			}
		}
	}
}

func readLine(b *bare.Reader) api.LineV2 {
	name, _ := b.ReadString()
	r, _ := b.ReadU8()
	g, _ := b.ReadU8()
	bc, _ := b.ReadU8()
	kind, _ := b.ReadUint()
	feedID, _ := b.ReadString()
	l, _ := b.ReadUint()
	hh := make([][]string, l)
	for i := 0; i < int(l); i++ {
		k, _ := b.ReadUint()
		hT := make([]string, k)
		for i := 0; i < int(k); i++ {
			h, _ := b.ReadString()
			hT[i] = h
		}
		hh[i] = hT
	}
	l, _ = b.ReadUint()
	graphs := make([]api.LineGraphV1, l)
	for i := 0; i < int(l); i++ {
		graphs[i] = readLineGraph(b)
	}

	return api.LineV2{
		name, api.ColourV1{r, g, bc}, api.LineTypeV3(kind), feedID, hh, graphs,
	}
}

func readLineGraph(b *bare.Reader) api.LineGraphV1 {
	l, _ := b.ReadUint()
	stops := make([]api.StopStubV1, l)
	for i := 0; i < int(l); i++ {
		code, _ := b.ReadString()
		name, _ := b.ReadString()
		nodeName, _ := b.ReadString()
		zone, _ := b.ReadString()
		onD, _ := b.ReadBool()
		stop := api.StopStubV1{
			code, name, nodeName, zone, onD,
		}
		stops[i] = stop
	}
	l, _ = b.ReadUint()
	nextN := map[int][]int{}
	for i := 0; i < int(l); i++ {
		k, _ := b.ReadInt()
		ll, _ := b.ReadUint()
		v := make([]int, ll)
		for j := 0; j < int(ll); j++ {
			vv, _ := b.ReadInt()
			v[j] = int(vv)
		}
		nextN[int(k)] = v
	}
	return api.LineGraphV1{
		stops, nextN,
	}
}

func readStop(b *bare.Reader) api.StopV2 {

	code, _ := b.ReadString()
	name, _ := b.ReadString()
	nodeName, _ := b.ReadString()
	zone, _ := b.ReadString()
	l, _ := b.ReadUint()
	feedID := make([]byte, l)
	b.ReadDataFixed(feedID)
	b.ReadF64() // lat
	b.ReadF64() // lon
	l, _ = b.ReadUint()
	changeOptions := make([]api.ChangeOptionV1, l)
	for j := 0; j < int(l); j++ {
		ll, _ := b.ReadUint()
		lineName := make([]byte, ll)
		b.ReadDataFixed(lineName)
		ll, _ = b.ReadUint()
		headsign := make([]byte, ll)
		b.ReadDataFixed(headsign)
		changeOptions[j] = api.ChangeOptionV1{string(lineName), string(headsign)}
	}
	return api.StopV2{
		code, name, nodeName, zone, string(feedID), api.PositionV1{}, changeOptions,
	}
}

func line(name string) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url+"lines/"+name, nil)
	req.Header.Add("Accept", "application/0+bare")
	response, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Getting")
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer response.Body.Close()
	b := bare.NewReader(response.Body)
	tag, _ := b.ReadUint()
	switch tag {
	case 0:
		line := readLine(b)
		fmt.Printf("%s [%v], %s, %v\n", line.Name, line.Colour, line.Kind.String(), line.Headsigns)
		fmt.Printf("%+v\n", line)
	default:
		fmt.Println(tag)
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "usage: bimba <type> <query>")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "f":
		getFeeds()
	case "q":
		query(os.Args[2], "q")
	case "line":
		line(os.Args[2])
	case "@":
		query(os.Args[2], "near")
	}
}
